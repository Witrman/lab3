import barBossHouse.*;
import barBossHouse.Abstract.MenuItem;
import barBossHouse.Immutable.Address;
import barBossHouse.Immutable.Customer;
import barBossHouse.Interface.Alcoholable;
import barBossHouse.Interface.Order;
import barBossHouse.Enum.DrinkTypeEnum;
import barBossHouse.Interface.OrdersManager;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Formatter;

public class Test {
    public static void main(String[] args) {


        Customer Iam = new Customer(LocalDate.of(1995, 12, 16), "Stas", "Vitman", new Address("ул. Московская", 2, 'A', 2));
        Order order = new TableOrder(-2, Iam);

        MenuItem dish = new Dish("Cezar", "odin,dva,tri,cheturi", 189);
        MenuItem drink = new Drink("Vodka Ice", "Vodka, Ice", 630, 40, DrinkTypeEnum.VODKA);

        for (int i = 0; i < 2; i++) {
            order.add(dish);
            order.add(dish);
            order.add(drink);
        }
        System.out.println("Первый заказ: " + order.toString());

        System.out.println("Общая стоимость: " + order.costTotal() +
                "\nВсего блюд Cezar: " + order.dishQuantity(dish) +
                "\nХэшкод заказа: " + order.hashCode());

        Alcoholable alco = (Alcoholable) drink;
        if (alco.isAlcoholicDrink())
            System.out.println("Напиток " + drink.getName() + " содержит " + alco.getAlcoholVol() + "% алкоголя");

        Order order1 = new InternetOrder(Iam, order.getMenuItems());
        order1.add(drink);
        order1.add(dish);
        System.out.println("Удалить напиток: " + order1.remove(drink) +
                "\nОбщая стоимость: " + order1.costTotal());
        Order orr = order1;

        Order[] ord = new Order[]{order1, order1, order1};
        OrdersManager ordersManager = new InternetOrdersManager(ord);
        System.out.println("\nСобрал интернет заказов: " + ordersManager.ordersQuantity() +
                "\nБлюд с названием Cezar: " + ordersManager.dishQuantity(dish) +
                "\nОбщая сумма: " + ordersManager.ordersCostSummery() +
                "\nзаказ 1 и 2 равны: " + order1.equals(orr));
        InternetOrder or = (InternetOrder) order1;
        System.out.println("\nИнтернет заказ: " + or.toString());
        ((InternetOrder) order1).remove("Cezar");
        ((InternetOrder) order1).removeAll("Vodka Ice");
        System.out.println(or.toString());

        order.setTimeOrder(LocalDateTime.of(1995, 12, 12, 13, 22));

        System.out.println(order.getTimeOrder().toString() + "\n мой возраст " + Iam.getBirthDate());

        System.out.println(ordersManager.getCountOrdersOfDate(LocalDate.now()));
        Iam = new Customer(LocalDate.of(2020, 12, 16), "Stas", "Vitman", new Address("Самара",121,"ул. Московская", 2, 'A', 2));
       // order.add(Drink);  эксэпшина выводится сразу )))))

    }
}
