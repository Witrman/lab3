package barBossHouse.Interface;

import barBossHouse.Abstract.MenuItem;
import barBossHouse.Immutable.Customer;

import java.time.LocalDate;
import java.util.Collection;

public interface OrdersManager extends Collection<Order> {
    int ordersQuantity();

    Order[] getOrders();

    double ordersCostSummery();

    int dishQuantity(String dishName);

    int dishQuantity(MenuItem menuItem);

    int getCountOrdersOfDate(LocalDate localDate);

    Order[] getOrdersOfDay(LocalDate localDate);

    Order[] getOrdersOfCustomer(Customer customer);

}
