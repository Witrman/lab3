package barBossHouse.Interface;

import barBossHouse.Abstract.MenuItem;
import barBossHouse.Immutable.Customer;

import java.time.LocalDateTime;
import java.util.List;

public interface Order extends List<MenuItem> {

    boolean add(MenuItem menuItem);

    void setTimeOrder(LocalDateTime timeOrder);

    LocalDateTime getTimeOrder();

    boolean remove(String dishName);

    boolean remove(MenuItem menuItem);

    int removeAll(String dishName);

    int removeAll(MenuItem menuItem);

    int dishQuantity();

    int dishQuantity(String dishName);

    int dishQuantity(MenuItem menuItem);

    MenuItem[] getMenuItems();

    double costTotal();

    Customer getCustomer();

    void setCustomer(Customer customer);

    String[] dishesNames();

    MenuItem[] sortedDishesByCostDesc();



    String toString();

    boolean equals(Object o);

    int hashCode();

}
