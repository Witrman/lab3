package barBossHouse.Interface;

public interface Alcoholable {

    boolean isAlcoholicDrink();


    double getAlcoholVol();

}
