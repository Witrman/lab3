package barBossHouse.Exceptions;

public class NegativeSizeException extends NegativeArraySizeException {

    private static final long serialVersionUID = -8960118058596222861L;
    public NegativeSizeException() {
    }

    public NegativeSizeException(String s) {
        super(s);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public void printStackTrace() {
        super.printStackTrace();
    }

}
