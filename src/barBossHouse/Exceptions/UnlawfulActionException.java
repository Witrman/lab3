package barBossHouse.Exceptions;

public class UnlawfulActionException extends Exception {
    public UnlawfulActionException() {
    }

    public UnlawfulActionException(String message) {
        super(message);
    }

    public UnlawfulActionException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnlawfulActionException(Throwable cause) {
        super(cause);
    }

    public UnlawfulActionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
