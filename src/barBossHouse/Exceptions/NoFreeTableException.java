package barBossHouse.Exceptions;

public class NoFreeTableException extends  Exception {
    public NoFreeTableException() {
    }

    public NoFreeTableException(String message) {
        super(message);
    }

    public NoFreeTableException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoFreeTableException(Throwable cause) {
        super(cause);
    }

    public NoFreeTableException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

