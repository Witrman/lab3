package barBossHouse;

import barBossHouse.Abstract.MenuItem;
import barBossHouse.Exceptions.AlreadyAddedException;
import barBossHouse.Exceptions.NegativeSizeException;
import barBossHouse.Exceptions.NoFreeTableException;
import barBossHouse.Immutable.Customer;
import barBossHouse.Interface.Order;
import barBossHouse.Interface.OrdersManager;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.function.Predicate;

public class TableOrderManager implements OrdersManager {
    private Order[] orders;
    private static final int DEFAULT_CAPACITY = 16;


    public TableOrderManager(int count) {
        try {
            if(count<=0)throw new NegativeSizeException("Обьем массива не может быть отрицательным");
        }catch(NegativeSizeException e){
            System.out.println(e.toString());
            count = DEFAULT_CAPACITY;
        }finally {
            orders = new Order[count];
        }
    }

    public int getCountOrdersOfDate(LocalDate localDate) {
        int count = 0;
        Order[] ord = getOrders();
        for (int i = 0; i < ord.length; i++) {
            if (ord[i].getTimeOrder().toLocalDate().equals(localDate)) {
                count++;
            }
        }
        return count;
    }

    public Order[] getOrdersOfDay(LocalDate localDate) {
        Order[] ord = getOrders();
        Order[] ordRez = new Order[ord.length];
        int h = 0;
        for (int i = 0; i < ord.length; i++) {
            if (ord[i].getTimeOrder().toLocalDate().equals(localDate)) {
                ordRez[h] = ord[i];
                h++;
            }
        }
        Order[] or = new Order[h];
        System.arraycopy(ordRez, 0, or, 0, h);
        return or;
    }

    public Order[] getOrdersOfCustomer(Customer customer)
    {
        Order[] ord = getOrders();
        Order[] ordRez = new Order[ord.length];
        int h = 0;
        for (int i = 0; i < ord.length; i++) {
            if (ord[i].getCustomer().equals(customer)) {
                ordRez[h] = ord[i];
                h++;
            }
        }
        Order[] or = new Order[h];
        System.arraycopy(ordRez, 0, or, 0, h);
        return or;
    }

    public void add(Order order, int tableNum) {
        try {
            if(orders[tableNum]!=null) throw new AlreadyAddedException("Заказ не может быть добавлен");

            orders[tableNum] = order;
        }catch (AlreadyAddedException e)
        {
            System.out.println(e.toString());
        }
        orders[tableNum] = order;
    }

    public Order getOrder(int tableNum) {
        return orders[tableNum];
    }

    public void addDish(MenuItem menuItem, int tableNum) {
        orders[tableNum].add(menuItem);
    }

    public void removeOrder(int tableNum) {
        orders[tableNum] = null;
    }

    public int freeTableNum() {
        int f =-1;
        for (int i = 0; i < orders.length; i++) {
            if (orders[i] == null) {
                f=i;
                return f;
            }
        }
        try {
           if(f<0) throw new NoFreeTableException("Свободных столов нет");
           } catch (NoFreeTableException e) {
            System.out.println(e.toString());
        }
        return f;
    }

    public int ordersQuantity() {
        Order[] bufOrders = getOrders();
        int count = 0;
        for (int i = 0; i < bufOrders.length; i++) {
            count += bufOrders[i].dishQuantity();
        }
        return count;
    }

    public int dishQuantity(MenuItem menuItem) {
        Order[] bufOrders = getOrders();
        int count = 0;
        for (int i = 0; i < bufOrders.length; i++) {
            count += bufOrders[i].dishQuantity(menuItem);
        }
        return count;
    }

    public int removeOrder(Order order) {
        for (int i = 0; i < orders.length; i++) {
            if (orders[i].equals(order)) {
                removeOrder(i);
                return i;
            }
        }
        return -1;
    }


    public int removeAllOrder(Order order) {
        int count = 0;
        for (int i = 0; i < orders.length; i++) {
            if (orders[i].equals(order)) {
                removeOrder(i);
                count++;
            }
        }
        if (count > 0) return count;
        return -1;
    }

    public int[] freeTableNums() {
        return predicateTableNums((order -> order == null));
    }

    public int[] noFreeTableNums() {
        return predicateTableNums((order -> order != null));
    }


    private int[] predicateTableNums(Predicate<Order> isNull) {
        int[] nums = new int[orders.length];
        int j = 0;
        for (int i = 0; i < nums.length && j < nums.length; i++) {
            if (isNull.test(orders[i])) {
                nums[j] = i;
                j++;
            }
        }
        int[] numsRez = new int[j];
        System.arraycopy(nums, 0, numsRez, 0, j);
        return numsRez;
    }


    public Order[] getOrders() {
        int[] noFT = noFreeTableNums();
        Order[] ordersBuf = new Order[noFT.length];
        for (int i = 0; i < noFT.length; i++) {
            ordersBuf[i] = orders[noFT[i]];
        }
        return ordersBuf;
    }

    public double ordersCostSummery() {
        double sum = 0;

        for (Order order : orders) {
            if (order != null) {
                sum += order.costTotal();
            }
        }
        return sum;
    }

    public int dishQuantity(String dishName) {
        int sum = 0;
        for (int i = 0; i < orders.length; i++) {
            if (orders[i] != null) {
                sum += orders[i].dishQuantity(dishName);
            }
        }
        return sum;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<Order> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Order order) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Order> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }
}
