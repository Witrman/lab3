package barBossHouse;

import barBossHouse.Abstract.MenuItem;
import barBossHouse.Abstract.util;

public class Dish extends MenuItem {
    private static final int DEFAULT_ZERO = 0;

    public Dish(String name, String description) {
        super(name, description, DEFAULT_ZERO);
    }

    public Dish(String name, String description, double cost) {
        super(name, description, cost);
    }

    @Override
    public String toString() {
        return String.format("%s%n      %s  ", super.toString() , getDescription());
    }

    @Override
    public int hashCode() {
        return super.hashCode() ^ (int) getCost();
    }


}
