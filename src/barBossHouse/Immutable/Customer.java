package barBossHouse.Immutable;

import barBossHouse.Abstract.util;

import java.time.LocalDate;

public final class Customer {
    private static final LocalDate DEFAULT_DATE = LocalDate.of(1995, 4, 21);
    private static final String DEFAULT_STRING = "";
    private String firstName;
    private String secondName;
    private LocalDate birthDate;
    private Address address;
    public static Customer MATURE_UNKNOWN_CUSTOMER = new Customer(DEFAULT_DATE);
    public static Customer NOT_MATURE_UNKNOWN_CUSTOMER = new Customer(LocalDate.of(2002, 1, 1));

    public Customer() {
        this(DEFAULT_DATE);
    }

    public Customer(LocalDate birthDate) {
        this(birthDate, DEFAULT_STRING, DEFAULT_STRING, Address.EMPTY_ADDRESS);
    }

    public Customer(LocalDate birthDate, String firstName, String secondName, Address address) {
        try {
            if(birthDate.isAfter(LocalDate.now())) throw new IllegalArgumentException("Дата рождения не возможна");
        } catch (IllegalArgumentException e) {
            System.out.println(e.toString());
        } finally {
            this.firstName = firstName;
            this.secondName = secondName;
            this.birthDate = birthDate;
            this.address = address;
        }
    }

    public Address getAddress() {
        return address;
    }

    public int getBirthDate() {
        return LocalDate.now().getYear() - birthDate.getYear();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Заказчик: ")
                .append(firstName).append(" ")
                .append(secondName).append(" ")
                .append(getBirthDate()).append(" ").append("\n  ").append(address.toString()).append(" ");
        return stringBuilder.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return birthDate == customer.birthDate &&
                firstName.equals(customer.firstName) &&
                secondName.equals(customer.secondName) &&
                address.equals(customer.address) &&
                getBirthDate() == ((Customer) o).getBirthDate();
    }

    @Override
    public int hashCode() {

        return firstName.hashCode() ^
                secondName.hashCode() ^
                birthDate.hashCode() ^
                address.hashCode();
    }
}
