package barBossHouse.Immutable;

import barBossHouse.Abstract.util;

public final class Address {

    private static final int DEFAULT_NUMBER = -1;
    private static final String DEFAULT_STRING = "";
    private static final char DEFAULT_CHAR = ' ';
    private static final String DEFAULT_CITY_NAME = "Самара";
    public static final Address EMPTY_ADDRESS = new Address();
    private String cityName;
    private int zipCode;
    private String streetName;
    private int buildingNumber;
    private char buildingLetter;
    private int apartamentNumber;

    public Address() {
        this(DEFAULT_STRING, DEFAULT_NUMBER, DEFAULT_STRING, DEFAULT_NUMBER, DEFAULT_CHAR, DEFAULT_NUMBER);

    }

    public Address(String streetName, int buildingNumber, char buildingLetter, int apartamentNumber) {
        this(DEFAULT_CITY_NAME, DEFAULT_NUMBER, streetName, buildingNumber, buildingLetter, apartamentNumber);
    }

    public Address(String cityName, int zipCode, String streetName, int buildingNumber, char buildingLetter, int apartmentNumber) {
        try {
            if (zipCode < 0) throw new IllegalArgumentException("Почтовый индекс не может быть отрицательным");
            if (buildingNumber < 0) throw new IllegalArgumentException("Номер здания не может быть отрицательным");
            if (apartmentNumber < 0) throw new IllegalArgumentException("Номер квартиры не может быть отрицательным");
            if (!Character.isLetter(buildingLetter))
                throw new IllegalArgumentException("Литера здания должна быть буквой");

        } catch (IllegalArgumentException e) {
            System.out.println(e.toString());


        } finally {
            this.cityName = cityName;
            this.zipCode = zipCode;
            this.streetName = streetName;
            this.buildingNumber = buildingNumber;
            this.buildingLetter = buildingLetter;
            this.apartamentNumber = apartmentNumber;
        }

    }

    public String getCityName() {
        return cityName;
    }

    public int getZipCode() {
        return zipCode;
    }

    public String getStreetName() {
        return streetName;
    }

    public int getBuildingNumber() {
        return buildingNumber;
    }

    public char getBuildingLetter() {
        return buildingLetter;
    }

    public int getApartamentNumber() {
        return apartamentNumber;
    }

    @Override
    public String toString() {
        return String.format("Адресс: %1s%1s%1s%1s%1s%1s", cityName,
                util.isNotEmptyInt(zipCode),
                streetName,
                util.isNotEmptyInt(buildingNumber),
                buildingLetter,
                util.isNotEmptyInt(apartamentNumber));
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return zipCode == address.zipCode &&
                buildingNumber == address.buildingNumber &&
                buildingLetter == address.buildingLetter &&
                apartamentNumber == address.apartamentNumber &&
                cityName.equals(address.cityName) &&
                streetName.equals(address.streetName);
    }

    @Override
    public int hashCode() {

        return cityName.hashCode() ^
                zipCode ^
                streetName.hashCode() ^
                buildingNumber ^
                buildingLetter ^
                apartamentNumber;
    }
}
