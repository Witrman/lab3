package barBossHouse;

import barBossHouse.Abstract.MenuItem;
import barBossHouse.Abstract.util;
import barBossHouse.Immutable.Customer;
import barBossHouse.Interface.Order;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import static java.time.LocalDateTime.*;


public class InternetOrder implements Order {
    private Customer customer;
    //TODO: неконстантные поля не инициализируют при объявлении
    private MenuItemList menuItems;
    private LocalDateTime timeOrder;
    private static final int DEFAULT_ZERO = 0;

    public InternetOrder() {
        this(new Customer(), new MenuItem[DEFAULT_ZERO]);

    }

    public InternetOrder(Customer customer, MenuItem[] menuIt) {
        this.customer = customer;
        menuItems = new MenuItemList();
        for (int i = 0; i < menuIt.length; i++) {
            menuItems.add(menuIt[i]);
        }
        timeOrder = now();

    }

    public void setTimeOrder(LocalDateTime timeOrder) {
        this.timeOrder = timeOrder;
    }

    public LocalDateTime getTimeOrder() {
        return timeOrder;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<MenuItem> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    public boolean add(MenuItem menuItem) {
        menuItems.add(menuItem);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends MenuItem> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends MenuItem> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }


    public boolean remove(MenuItem menuItem) {
        return menuItems.remove(menuItem);

    }

    public boolean remove(String menuItem) {
        return menuItems.remove(menuItem);

    }

    public int removeAll(MenuItem menuItem) {
        return menuItems.removeAll(menuItem);

    }

    public int removeAll(String menuItem) {
        return menuItems.removeAll(menuItem);

    }

    public int dishQuantity() {
        return menuItems.getSize();
    }

    public MenuItem[] getMenuItems() {
        return menuItems.toArray();
    }

    public double costTotal() {
        double sum = 0;
        for (MenuItem aMi : this.getMenuItems()) {
            sum += aMi.getCost();
        }
        return sum;
    }

    public int dishQuantity(String ItemName) {
        int count = 0;
        for (MenuItem aMi : this.getMenuItems()) {
            if (aMi.getName().equals(ItemName)) {
                count++;
            }
        }
        return count;
    }

    public int dishQuantity(MenuItem ItemName) {
        int count = 0;
        for (MenuItem aMi : this.getMenuItems()) {
            if (aMi.equals(ItemName)) {
                count++;
            }
        }
        return count;
    }

    public String[] dishesNames() {
        MenuItem[] mi = this.getMenuItems();
        int h = 0;
        String[] dishNames = new String[mi.length];
        for (int i = 0; i < mi.length; i++) {
            if (!contains(dishNames, mi[i].getName())) {
                dishNames[h++] = mi[i].getName();
            }
        }
        String[] dishNamesResult = new String[h];
        System.arraycopy(dishNames, 0, dishNamesResult, 0, dishNamesResult.length);
        return dishNamesResult;
    }

    private boolean contains(String[] dishNames, String dishName) {
        for (int i = 0; i < dishName.length(); i++) {
            if (dishNames[i].equals(dishName))
                return true;
        }
        return false;
    }

    public MenuItem[] sortedDishesByCostDesc() {
        MenuItem[] menuItem = getMenuItems();
        return util.sortMerge(menuItem);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        MenuItem[] mi = menuItems.toArray();
        sb.append(customer.toString()).append("\n")
                .append(dishQuantity()).append("\n");
        for (int i = 0; i < dishQuantity(); i++) {
            sb.append(mi[i].toString()).append("\n");
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InternetOrder that = (InternetOrder) o;
        if (!customer.equals(that.customer) || (menuItems.getSize() != that.menuItems.getSize())) return false;
        if(!getTimeOrder().equals(((InternetOrder) o).getTimeOrder())) return false;
        MenuItem[] m1 = that.menuItems.toArray();
        MenuItem[] m2 = this.menuItems.toArray();
        for (int i = 0; i < menuItems.getSize(); i++) {
            if (!m1[i].equals(m2[i])) return false;
        }
        return true;
    }

    @Override
    public int hashCode() {

        return customer.hashCode()
                ^ menuItems.hashCode();
    }

    @Override
    public MenuItem get(int index) {
        return null;
    }

    @Override
    public MenuItem set(int index, MenuItem element) {
        return null;
    }

    @Override
    public void add(int index, MenuItem element) {

    }

    @Override
    public MenuItem remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<MenuItem> listIterator() {
        return null;
    }

    @Override
    public ListIterator<MenuItem> listIterator(int index) {
        return null;
    }

    @Override
    public List<MenuItem> subList(int fromIndex, int toIndex) {
        return null;
    }
}
