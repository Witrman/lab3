package barBossHouse;

import barBossHouse.Abstract.MenuItem;
import barBossHouse.Exceptions.AlreadyAddedException;
import barBossHouse.Immutable.Customer;
import barBossHouse.Interface.Order;
import barBossHouse.Interface.OrdersManager;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Iterator;

public class InternetOrdersManager implements OrdersManager {
    private QueueNode head;
    private QueueNode tail;
    private int size;

    private static final int DEFAULT_SIZE = 0;

    public InternetOrdersManager() {
        head = new QueueNode();
        tail = head;
        size = DEFAULT_SIZE;
    }

    public InternetOrdersManager(Order[] orders) {
        this();
        for (Order order : orders) {
            add(order);
        }
    }

    public int getCountOrdersOfDate(LocalDate localDate) {
        QueueNode currentNode = head;
        int count = 0;
        for (int i = 0; i < size; i++) {
            if (currentNode.value.getTimeOrder().toLocalDate().equals(localDate)) count++;
            currentNode = currentNode.next;
        }
        return count;
    }

    public Order[] getOrdersOfDay(LocalDate localDate) {
        Order[] ord;
        ord = getOrders();
        Order[] ordRez = new Order[ord.length];
        int h = 0;
        for (int i = 0; i < ord.length; i++) {
            if (ord[i].getTimeOrder().toLocalDate().equals(localDate)) {
                ordRez[h] = ord[i];
                h++;
            }
        }
        Order[] orr = new Order[h];
        System.arraycopy(ordRez, 0, orr, 0, h);
        return orr;
    }

    public Order[] getOrdersOfCustomer(Customer customer) {
        Order[] ord;
        ord = getOrders();
        Order[] ordRez = new Order[ord.length];
        int h = 0;
        for (int i = 0; i < ord.length; i++) {
            if (ord[i].getCustomer().equals(customer)) {
                ordRez[h] = ord[i];
                h++;
            }
        }
        Order[] or = new Order[h];
        System.arraycopy(ordRez, 0, or, 0, h);
        return or;
    }


    public int ordersQuantity() {
        return size;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<Order> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    public boolean add(Order order) {
        try {
            if(isMade(order)) throw new AlreadyAddedException("Заказ не может быть сделан");
            if (tail.value == null) {
                tail.value = order;
            } else {
                QueueNode nextNode = new QueueNode(order);
                nextNode.prev = tail;
                tail.next = nextNode;
                tail = nextNode;
            }
            size++;
            return true;
        } catch (AlreadyAddedException e) {
            System.out.println(e.toString());
            return false;
        }
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Order> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    private boolean isMade(Order order)
    {
        Order[] orders = getOrders();
        for (int i = 0; i <orders.length ; i++) {
            if(orders[i].getCustomer().equals(order.getCustomer())||
                    orders[i].getTimeOrder().equals(order.getTimeOrder()))
                return true;
        }
        return false;
    }

    public Order getFirst() {
        return head.value;
    }

    public Order popFirst() {
        if (size == 0) return null;
        QueueNode oldHead = head;
        head = head.next;

        if (oldHead.equals(tail)) {
            head = new QueueNode();
            tail = head;
        }

        head.prev = null;

        size--;
        return oldHead.value;
    }

    public Order[] getOrders() {
        Order[] orders = new Order[size];
        QueueNode currentNode = head;
        for (int i = 0; i < size; i++) {
            orders[i] = currentNode.value;
            currentNode = currentNode.next;
        }
        return orders;
    }

    public int dishQuantity(MenuItem menuItem) {
        QueueNode currentNode = head;
        int count = 0;
        for (int i = 0; i < size; i++) {
            count += currentNode.value.dishQuantity(menuItem);
            currentNode = currentNode.next;
        }
        return count;
    }

    public double ordersCostSummery() {
        double sum = 0;
        QueueNode currentNode = head;
        for (int i = 0; i < size; i++) {
            sum += currentNode.value.costTotal();
            currentNode = currentNode.next;
        }
        return sum;
    }

    public int dishQuantity(String dishName) {
        int sum = 0;

        QueueNode currentNode = head;
        for (int i = 0; i < size; i++) {
            sum += currentNode.value.dishQuantity(dishName);
            currentNode = currentNode.next;
        }
        return sum;
    }

    private class QueueNode {
        QueueNode next;
        QueueNode prev;
        Order value;

        QueueNode() {
        }

        QueueNode(Order value) {
            this.value = value;
        }
    }
}