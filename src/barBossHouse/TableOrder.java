package barBossHouse;

import barBossHouse.Abstract.MenuItem;

import barBossHouse.Exceptions.NegativeSizeException;
import barBossHouse.Exceptions.UnlawfulActionException;
import barBossHouse.Immutable.Customer;
import barBossHouse.Interface.Order;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Consumer;

import static java.time.LocalDateTime.*;


public class TableOrder implements Order {
    private int size;
    private MenuItem[] menuItems;
    private Customer customer;
    private LocalDateTime timeOrder;
    private static final int DEFAULT_SIZE = 0;
    private static final int DEFAULT_CAPACITY = 16;

    public TableOrder() {
        this(DEFAULT_CAPACITY, new Customer());
    }

    public TableOrder(int size, Customer customer) {
        try {
            if (size <= 0) throw new NegativeSizeException("Обьем массива не может быть отрицательным");
        } catch (NegativeSizeException e) {
            System.out.println(e.toString());
            size = DEFAULT_CAPACITY;
        } finally {
            menuItems = new MenuItem[size];
            this.size = DEFAULT_SIZE;
            this.customer = customer;
            timeOrder = now();
        }
    }

    public TableOrder(MenuItem[] menuItems, Customer customer) {
        this.menuItems = menuItems;
        this.size = DEFAULT_SIZE;
        this.customer = customer;
        timeOrder = now();
    }

    public void setTimeOrder(LocalDateTime timeOrder) {
        this.timeOrder = timeOrder;
    }

    public LocalDateTime getTimeOrder() {
        return timeOrder;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object menuItem) {
        return dishQuantity((MenuItem) menuItem) > 0;
    }

    @Override
    public Iterator<MenuItem> iterator() {
        return new Itr();
    }

    @Override
    public Object[] toArray() {
        return getMenuItems();
    }

    //todo
    @Override
    public <T> T[] toArray(T[] a) {
        if (a.length < size)
            return (T[]) Arrays.copyOf(menuItems, size, a.getClass());
        System.arraycopy(menuItems, 0, a, 0, size);
        if (a.length > size)
            a[size] = null;
        return a;
    }

    public boolean add(MenuItem menuItem) {
        try {
            if (customer.getBirthDate() < 18 || timeOrder.getHour() >= 22)
                throw new UnlawfulActionException("Алкоголь невозможно добавить в заказ");
            if (size == menuItems.length)
                increaseArray();
            menuItems[size] = menuItem;
            size++;
            return true;
        } catch (UnlawfulActionException e) {
            System.out.println(e.toString());
            return false;
        }

    }

    @Override
    public boolean remove(Object o) {
        return remove((MenuItem) o);
    }

    public boolean remove(MenuItem menuItem) {
        for (int i = 0; i < size; i++) {

            if (menuItems[i].equals(menuItem)) {
                for (int j = i; j < size; j++) {
                    menuItems[j] = menuItems[j + 1];
                    if (j == size) {
                        menuItems[j + 1] = null;
                    }
                }
                size--;
                return true;
            }
        }
        return false;
    }

    //todo
    @Override
    public boolean containsAll(Collection<?> c) {
        return this.equals(c);
    }

    @Override
    public boolean addAll(Collection<? extends MenuItem> c) {
        return addAll(size, c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends MenuItem> c) {
        Object[] menuItems = c.toArray();
        if (menuItems.length == 0) return false;
        for (Object menuItem : menuItems) {
            this.add(index++, (MenuItem) menuItem);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        Object[] menuItems = c.toArray();

        for (Object menuItem : menuItems) {
            removeAll((MenuItem) menuItem);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        Object[] menuItems = c.toArray();
        boolean result = false;
        for (Object menuItem : menuItems) {
            if (this.dishQuantity((MenuItem) menuItem) > 0) {
                this.remove((MenuItem) menuItem);
                result = true;
            }
        }
        return result;
    }


    @Override
    public void clear() {
        new TableOrder();
    }

    private void increaseArray() {
        MenuItem[] newMenuItems = new MenuItem[menuItems.length * 2];
        System.arraycopy(menuItems, 0, newMenuItems, 0, menuItems.length);
        menuItems = newMenuItems;
    }

    public boolean remove(String dishName) {
        for (int i = 0; i < size; i++) {
            if (menuItems[i].getName().equals(dishName)) {
                for (int j = i; j < size; j++) {
                    menuItems[j] = menuItems[j + 1];
                    if (j == size) {

                        menuItems[j + 1] = null;
                    }
                }
                size--;
                return true;
            }
        }
        return false;
    }

    public int removeAll(String dishName) {
        int count = 0;
        while (remove(dishName)) {
            count++;
        }
        return count;
    }

    public int removeAll(MenuItem menuItem) {
        int count = 0;
        while (remove(menuItem)) {
            count++;
        }
        return count;
    }

    public int dishQuantity() {
        return size;
    }

    public int dishQuantity(String dishName) {
        int count = 0;
        for (int i = 0; i < size; i++) {
            if (menuItems[i].getName().equals(dishName)) {
                count++;
            }
        }
        return count;
    }

    public int dishQuantity(MenuItem menuItem) {
        int count = 0;
        for (int i = 0; i < size; i++) {
            if (menuItems[i].equals(menuItem)) {
                count++;
            }
        }
        return count;
    }

    public MenuItem[] getMenuItems() {
        MenuItem[] menuItems = new MenuItem[this.size];
        System.arraycopy(this.menuItems, 0, menuItems, 0, menuItems.length);
        return menuItems;
    }

    public double costTotal() {
        double sum = 0;
        for (int i = 0; i < size; i++) {
            sum += menuItems[i].getCost();
        }
        return sum;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String[] dishesNames() {
        int h = 0;
        String[] dishNames = new String[size];
        for (int i = 0; i < size; i++) {
            if (!contains(dishNames, menuItems[i].getName())) {
                dishNames[h++] = menuItems[i].getName();
            }
        }
        String[] dishNamesResult = new String[h];
        System.arraycopy(dishNames, 0, dishNamesResult, 0, dishNamesResult.length);
        return dishNamesResult;
    }

    private boolean contains(String[] dishNames, String dishName) {
        for (int i = 0; i < dishName.length(); i++) {
            if (dishNames[i].equals(dishName))
                return true;
        }
        return false;
    }

    public MenuItem[] sortedDishesByCostDesc() {

        menuItems = getMenuItems();
        Arrays.sort(menuItems);
        return menuItems;
    }


    @Override
    public String toString() {
        StringBuilder tableString = new StringBuilder();
        tableString.append("Заказ: ").append(size).append("\n");
        for (int i = 0; i < size; i++) {
            tableString.append(menuItems[i].toString()).append("\n");
        }
        return tableString.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TableOrder that = (TableOrder) o;
        return that.getTimeOrder().equals(((TableOrder) o).getTimeOrder())
                && size == that.size
                && Arrays.equals(menuItems, that.menuItems)
                && customer.equals(that.customer);
    }

    @Override
    public int hashCode() {

        return Arrays.hashCode(menuItems) ^
                size ^
                customer.hashCode();
    }

    @Override
    public MenuItem get(int index) {
        return menuItems[index];
    }

    @Override
    public MenuItem set(int index, MenuItem element) {
        MenuItem m = menuItems[index];
        menuItems[index] = element;
        return m;
    }

    @Override
    public void add(int index, MenuItem element) {
        if (size == menuItems.length)
            increaseArray();
        System.arraycopy(menuItems, index, menuItems, index + 1, size - index);
        menuItems[index] = element;
        size++;
    }

    @Override
    public MenuItem remove(int index) {
        MenuItem menuItem = get(index);
        remove(menuItem);
        return menuItem;
    }

    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < menuItems.length; i++) {
            if (menuItems[i].equals(o)) return i;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        int sum = -1;
        for (int i = 0; i < menuItems.length; i++) {
            if (menuItems[i].equals(o)) sum = i;
        }
        return sum;
    }

    @Override
    public ListIterator<MenuItem> listIterator() {
        return new ListItr(0);
    }

    @Override
    public ListIterator<MenuItem> listIterator(int index) {
        return new ListItr(index);
    }

    @Override
    public List<MenuItem> subList(int fromIndex, int toIndex) {
        List<MenuItem> list = null;

        return null;
    }


    private class Itr implements Iterator<MenuItem> {
        int cursor;
        int lastRet = -1;

        public Itr() {
        }

        @Override
        public boolean hasNext() {
            return cursor != size;
        }

        @Override
        public MenuItem next() {
            int i = cursor;
            if (i >= size)
                throw new NoSuchElementException();
            MenuItem[] menuItems = TableOrder.this.menuItems;
            if (i >= menuItems.length)
                throw new ConcurrentModificationException();
            cursor = i + 1;
            return menuItems[i];
        }

        @Override
        public void forEachRemaining(Consumer<? super MenuItem> action) {
            Objects.requireNonNull(action);
            final int size = TableOrder.this.size;
            int i = cursor;
            if (i < size) {
                final MenuItem[] menuItems = TableOrder.this.menuItems;
                if (i >= menuItems.length)
                    throw new ConcurrentModificationException();
                for (; i < size; i++)
                    action.accept(menuItems[i]);
                cursor = i;
                lastRet = i - 1;
            }
        }
    }

    private class ListItr extends Itr implements ListIterator<MenuItem> {
        ListItr(int index) {
            super();
            cursor = index;
        }

        @Override
        public boolean hasPrevious() {
            return cursor != 0;
        }

        @Override
        public MenuItem previous() {
            int i = cursor - 1;
            if (i < 0)
                throw new NoSuchElementException();
            MenuItem[] menuItems = TableOrder.this.menuItems;
            if (i >= menuItems.length)
                throw new ConcurrentModificationException();
            cursor = i;
            return menuItems[lastRet = i];
        }

        @Override
        public int nextIndex() {
            return cursor;
        }

        @Override
        public int previousIndex() {
            return cursor - 1;
        }

        @Override
        public void remove() {

        }

        @Override
        public void set(MenuItem menuItem) {
            if (lastRet < 0)
                throw new IllegalStateException();


            try {
                TableOrder.this.set(lastRet, menuItem);
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public void add(MenuItem menuItem) {
            try {
                int i = cursor;
                TableOrder.this.add(i, menuItem);
                cursor = i + 1;
                lastRet = -1;

            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
