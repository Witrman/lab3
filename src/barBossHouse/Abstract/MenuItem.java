package barBossHouse.Abstract;


public abstract class MenuItem implements Comparable<MenuItem> {
    private double cost;
    private String name;
    private String description;
    private static final int DEFAULT_COST = 0;

    protected MenuItem(String name, String description) {
        this(name, description, DEFAULT_COST);
    }

    protected MenuItem(String name, String description, double cost) {
       try {
           if(cost<0)  throw new IllegalArgumentException("Стоимость не может быть отрицательной");
       }catch (IllegalArgumentException e){
           System.out.println(e.toString());
           cost = DEFAULT_COST;
       }finally {

           this.name = name;
           this.description = description;
           this.cost = cost;
       }
    }

    public String getName() {
        return name;
    }


    public double getCost() {
        return cost;
    }

    @Override
    public int compareTo(MenuItem o) {
        if(this.getCost()>o.getCost()) return 1;
        if(this.getCost()<o.getCost()) return -1;
        return 0;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return String.format("Пункт меню: %s %1.1fр. " , name ,
                cost );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuItem menuItem = (MenuItem) o;
        return Double.compare(menuItem.cost, cost) == 0 &&
                name.equals(menuItem.name);
    }

    @Override
    public int hashCode() {

        return name.hashCode() ^ (int) cost;
    }
}
