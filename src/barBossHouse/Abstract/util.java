package barBossHouse.Abstract;

//TODO: очень странный класс с очень странными методами и именем
public abstract class util {

    public static String isNotEmptyInt(int i) {
        if (i == -1) return "";
        return " " + i;
    }

    public static MenuItem[] sortMerge(MenuItem[] arr) {
        int len = arr.length;
        if (len < 2) return arr;
        int middle = len / 2;

        return merge(sortMerge(splitArray(arr, 0, middle)),
                sortMerge(splitArray(arr, middle, len)));
    }

    private static MenuItem[] merge(MenuItem[] array1, MenuItem[] array2) {
        int length1 = array1.length;
        int length2 = array2.length;
        int a = 0, b = 0;
        int len = length1 + length2;
        MenuItem[] result = new MenuItem[len];
        for (int i = 0; i < len; i++) {
            if (b < length2 && a < length1) {
                if (array1[a].getCost() > array2[b].getCost())
                    result[i] = array2[b++];
                else result[i] = array1[a++];
            } else if (b < length2) {
                result[i] = array2[b++];
            } else {
                result[i] = array1[a++];
            }
        }

        return result;
    }

    private static MenuItem[] splitArray(MenuItem[] original, int from, int to) {
        MenuItem[] copy = new MenuItem[to - from];
        System.arraycopy(original, from, copy, 0,
                Math.min(original.length - from, to - from));
        return copy;
    }
}
