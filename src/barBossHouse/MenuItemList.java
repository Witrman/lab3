package barBossHouse;

import barBossHouse.Abstract.MenuItem;

import java.util.function.Predicate;

public class MenuItemList {
    private int size;
    private ListNode head;
    private ListNode tail;
    private static final int DEFAULT_ZERO = 0;

    public MenuItemList() {
        head = new ListNode();
        tail = head;
        this.size = DEFAULT_ZERO;
    }

    public int getSize() {
        return size;
    }

    public boolean add(MenuItem menuItem) {
        if (tail.value == null) {
            tail.value = menuItem;
        } else {
            ListNode newNode = new ListNode(menuItem);
            tail.next = newNode;
            tail = newNode;
        }
        size++;
        return true;
    }

    private int itemQuantity(Object object)
    {
        ListNode currentNode = head;
        for (int i = 0; i < size; i++) {
            if(String.class == object.getClass())
            {
                if(object.equals(currentNode.value.getName()))
                {
                    return  i;
                }
            } else {
                if(object.equals(currentNode.value))
                {
                    return  i;
                }
            }
            currentNode = currentNode.next;
        }
        return -1;
    }

    public boolean remove(String name) {
        return  remove(itemQuantity(name));
    }

    public boolean remove(MenuItem menuItem) {
        return  remove(itemQuantity(menuItem));
    }

    public int removeAll(String name) {
       int count = 0;
        while ( remove(itemQuantity(name)))
        { count++;}

        return count;
    }

    public int removeAll(MenuItem menuItem) {
        int count = 0;
        while ( remove(itemQuantity(menuItem)))
        { count++;}

        return count;
    }


    //TODO: очень странно использовать предикаты и флаг в качестве управления выполнением.
    public boolean remove(int indexNode)
    {
        ListNode currentNode = head;
        if(indexNode==0)
        {
            head = head.next;
            size--;
            return true;
        }
        for (int i = 0; i < size-1 ; i++) {
            if(i==indexNode-1)
            {
                currentNode.next = currentNode.next.next;
                size--;
                return true;
            }
            currentNode = currentNode.next;
        }
        return false;
    }

    public MenuItem[] toArray() {
        MenuItem[] menuItems = new MenuItem[size];
        ListNode currentNode = head;
        for (int i = 0; i < size; i++) {
            menuItems[i] = currentNode.value;
            currentNode = currentNode.next;
        }
        return menuItems;
    }

    @Override
    public int hashCode() {
        return head.hashCode() ^ tail.hashCode();
    }

    private class ListNode {
        ListNode next;
        MenuItem value;

        ListNode() {
        }

        ListNode(MenuItem value) {
            this.value = value;
        }
    }
}
