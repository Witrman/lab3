package barBossHouse;

import barBossHouse.Abstract.MenuItem;
import barBossHouse.Abstract.util;
import barBossHouse.Enum.DrinkTypeEnum;
import barBossHouse.Interface.Alcoholable;


public class Drink extends MenuItem implements Alcoholable {
    private double alcoholVol;
    private DrinkTypeEnum type;
    private static final int DEFAULT_ZERO = 0;
    private static final String DEFAULT_STRING = "";

    public Drink(String name, DrinkTypeEnum type) {
        this(name, DEFAULT_STRING, DEFAULT_ZERO, DEFAULT_ZERO, type);
    }

    public Drink(String name, String description, double cost, DrinkTypeEnum type) {
        this(name, description, cost, DEFAULT_ZERO, type);
    }

    public Drink(String name, String description, double cost, double alcoholVol, DrinkTypeEnum type) {

        super(name, description, cost);
        try {
            if(alcoholVol>100 || alcoholVol<0) throw new IllegalArgumentException("Доля алкоголя невозможна");
        } catch (IllegalArgumentException e) {
            System.out.println(e.toString());

        } finally {

            this.alcoholVol = alcoholVol;
            this.type = type;
        }
    }

    public DrinkTypeEnum getType() {
        return type;
    }

    @Override
    public String toString() {
        String alco = " Алкоголь: " + alcoholVol + "%";
        if (!isAlcoholicDrink()) alco = "";
        return String.format("Напиток: %s%n      %s%s%n      %s",
                type.toString(),
                super.toString(), alco,
                super.getDescription());
    }


    @Override
    public boolean isAlcoholicDrink() {
        return alcoholVol > 0;
    }

    @Override
    public double getAlcoholVol() {
        return alcoholVol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Drink drink = (Drink) o;
        return Double.compare(drink.alcoholVol, alcoholVol) == 0 &&
                type == drink.type;
    }

    @Override
    public int hashCode() {

        return super.hashCode() ^ (int) alcoholVol ^ type.hashCode();
    }

}
